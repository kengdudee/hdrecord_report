\vspace {-0.5cm}
\nobreakspace {}\hfill \textnormal {หน้า\\}\par 
\select@language {thai}
\contheading 
\contentsline {section}{บทคัดย่อ}{(1)}{table.caption.2}% 
\vspace {\baselineskip }
\contentsline {section}{Abstract}{(2)}{table.caption.3}% 
\vspace {\baselineskip }
\contentsline {section}{กิตติกรรมประกาศ}{(3)}{chapter*.4}% 
\vspace {\baselineskip }
\contentsline {section}{สารบัญรูป}{(6)}{chapter*.5}% 
\vspace {\baselineskip }
\contentsline {section}{สารบัญตาราง}{(9)}{chapter*.6}% 
\vspace {\baselineskip }
\contentsline {section}{สัญลักษณ์และคำย่อ}{(10)}{chapter*.7}% 
\addvspace {15\p@ }
\contentsline {chapter}{\numberline {1}บทนำ}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}ที่มาและความสำคัญ}{1}{section.1.1}% 
\contentsline {section}{\numberline {1.2}วัตถุประสงค์}{2}{section.1.2}% 
\contentsline {section}{\numberline {1.3}ขอบเขตการดำเนินงาน}{2}{section.1.3}% 
\contentsline {section}{\numberline {1.4}ขั้นตอนการดำเนินงาน}{2}{section.1.4}% 
\contentsline {section}{\numberline {1.5}ผลที่คาดว่าจะได้รับ}{3}{section.1.5}% 
\contentsline {section}{\numberline {1.6}ตารางการดำเนินงาน}{4}{section.1.6}% 
\addvspace {15\p@ }
\contentsline {chapter}{\numberline {2}วรรณกรรมและงานวิจัยที่เกี่ยวข้อง}{6}{chapter.2}% 
\contentsline {section}{\numberline {2.1}โรคไตวายเรื้อรัง}{6}{section.2.1}% 
\contentsline {section}{\numberline {2.2}การรักษาผู้ป่วยโรคไตเรื้อรังระยะสุดท้าย}{6}{section.2.2}% 
\contentsline {section}{\numberline {2.3}การประยุกต์ใช้แอปพลิเคชันบนอุปกรณ์พกพาในการบันทึกข้อมูลของผู้ป่วย}{7}{section.2.3}% 
\addvspace {15\p@ }
\contentsline {chapter}{\numberline {3}การดำเนินงาน}{8}{chapter.3}% 
\contentsline {section}{\numberline {3.1}ออกแบบส่วนต่อประสานกับผู้ใช้ (user interface, UI) และประสบการณ์ของผู้ใช้ (user experience, UX) ของแอปพลิเคชัน}{8}{section.3.1}% 
\contentsline {section}{\numberline {3.2}ออกแบบฐานข้อมูล}{47}{section.3.2}% 
\contentsline {section}{\numberline {3.3}พัฒนาแอปพลิเคชัน}{48}{section.3.3}% 
\addvspace {15\p@ }
\contentsline {chapter}{\numberline {4}ผลการดำเนินงาน}{49}{chapter.4}% 
\contentsline {section}{\numberline {4.1}การทำงานของแอปพลิเคชัน}{49}{section.4.1}% 
\addvspace {15\p@ }
\contentsline {chapter}{\numberline {5}สรุปโครงงาน}{59}{chapter.5}% 
\contentsline {section}{\numberline {5.1}สรุปผลโครงงาน}{59}{section.5.1}% 
\contentsline {section}{\numberline {5.2}อุปสรรคในการดำเนินงาน}{59}{section.5.2}% 
\contentsline {section}{\numberline {5.3}การนำมาพัฒนาต่อในอนาคต}{59}{section.5.3}% 
\addvspace {15\p@ }
\contentsline {section}{รายการอ้างอิง}{61}{Item.151}% 
\contentsline {section}{ภาคผนวก ก: วิธีการติดตั้งสำหรับผู้ดูแลระบบ}{63}{appendix.A}% 
\contentsline {section}{\numberline {ก.1}ติดตั้งฐานข้อมูล MySQL}{63}{section.A.1}% 
\contentsline {section}{\numberline {ก.2}สร้างฐานข้อมูล Hemodialysis record และสร้างตาราง}{63}{section.A.2}% 
\contentsline {section}{\numberline {ก.3}ติดตั้ง NodeJS และ ExpressJS}{70}{section.A.3}% 
\contentsline {section}{\numberline {ก.4}เชื่อมต่อกับฐานข้อมูล และรัน server ด้วย node}{70}{section.A.4}% 
\contentsfinish 
